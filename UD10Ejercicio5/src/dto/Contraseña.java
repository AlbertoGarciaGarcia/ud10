package dto;

public class Contraseña {

	 private final static int LongitudDefault =8; // Establecemos la longitud default
	 
	 private int longitud; // Y el que pasaremos
	 
	 private String contraseña;
	 
	 
	 public int getLongitud() { // Conseguimos la longitud
	        return longitud;
	    }
	 
	    public void setLongitud(int longitud) { // La establecemos a la conseguida
	        this.longitud = longitud;
	    }
	
	    public String getContraseña() { // Un get para conseguir la contraseña
	        return contraseña;
	    }
	    
	    public Contraseña (){
	        this(LongitudDefault);
	    }
	    
	    public Contraseña (int longitud){
	        this.longitud=longitud;
	        contraseña=generaPassword();
	    }
	   
	    
	    public int eleccionRandom() {
	    	
	    	int num=(int)Math.floor(Math.random()*(1-(3+1))+(3+1)); // Hacemos un metodo con un valor del 1 al 3 random
			
	        return num;
	    	
	    }
	    
	    public char generarCaracter() {
	    	
	    	if(eleccionRandom() == 1) { // Dependiendo del valor random de la funcion anterior
	    		char minusculas=(char)((int)Math.floor(Math.random()*(123-97)+97)); // Se añade una minuscula con los valores ASCII
	    		return minusculas;
	    	}
	    	else if(eleccionRandom() == 2) {
	    		 char mayusculas=(char)((int)Math.floor(Math.random()*(91-65)+65)); // Lo mismo con las myusculas
	    		 return mayusculas;
	    	}
	    	else {
	    		char numeros=(char)((int)Math.floor(Math.random()*(58-48)+48)); // Y con los numeros
	    		return numeros;
	    	}
	    	
	    	  
	    }    
	            
	    public String generaPassword (){
	        String contraseña ="";
	        for (int i=0;i<longitud;i++){
	            int eleccion= eleccionRandom();
	  
	            if (eleccion==1){ // Dependiendo del valor pasado anteriormente, añadiremos el valor a la contraseña
	                
	                contraseña = contraseña + generarCaracter();
	            }else if (eleccion==2) {
	              
	                   
	                    contraseña = contraseña + generarCaracter();
	                }
	            else{
	                  
	                    contraseña = contraseña + generarCaracter();
	                }
	            
	        }
	        return contraseña; // Y la devolvremos
	    }
	    
	    
	    
	    
	    public boolean esFuerte(){
	        int numerosTotal=0; // Haremos un total de tantos numeros, mayusculas y minuisculos que hay
	        int minusculasTotal=0;
	        int mayusculasTotal=0;
	     
	        for (int i=0;i<contraseña.length();i++){
	                if (contraseña.charAt(i)>=97 && contraseña.charAt(i)<=122) { // Con la funcion charAt que nos da el valor de la posicion del String
	                	  minusculasTotal = minusculasTotal + 1;
	                }															// Comprobaremos que tipo de char es y lo añadiremos al total
	                 
	                else if(contraseña.charAt(i)>=65 && contraseña.charAt(i)<=90){
	                	  mayusculasTotal = mayusculasTotal + 1;
	                }	                	                    	                      
	             
	                else {
	                    numerosTotal = numerosTotal + 1;	
	                }                                                           
	    }
	        if (numerosTotal>=5 && minusculasTotal>=1 && mayusculasTotal>=2){ // Ahora con los requisitos establecidos en el enunciado
	        																 // Devolveremos false o true dependiendo de la contraseña
	            return true;
	        }else{
	            return false;
	        }	        	 
	 
	    }
}
