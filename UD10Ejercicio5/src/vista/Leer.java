package vista;

import java.util.InputMismatchException;
import java.util.Scanner;
import excepciones.Excepciones;
import dto.Contrase�a;

public class Leer {
	public static void construirContrase�a() throws Excepciones {
		try {
			
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduce el tama�o del array de contrase�as");
		int tama�oArray = scanner.nextInt(); // Pediremos el tama�o del array
		
		Contrase�a arrayContrase�a[] = new Contrase�a[tama�oArray];
		System.out.println("Introduce la longitud de la contrase�a");
		int longitud = scanner.nextInt(); // Y el tama�o de la contrase�a teniendo en cuenta con una excepcion que no puede ser menor de 8
		if(longitud < 8) {
			 throw new excepciones.Excepciones("numeroMenor8");
		}
		boolean esFuerteONo[]=new boolean[tama�oArray]; // Crearemos un array con el mismo tama�o que el del anterior
		
		  for(int i=0;i<arrayContrase�a.length;i++){
	            arrayContrase�a[i]=new Contrase�a(longitud); // Asignaremos las posiciones de cada uno a cada objeto de contrase�a
	            esFuerteONo[i]=arrayContrase�a[i].esFuerte();
	            System.out.println("La fortaleza de la contrase�a : " + arrayContrase�a[i].getContrase�a()+" es  "+esFuerteONo[i]); // E impriremos el resultado
	        }
		
		
	}catch(Excepciones ex) {
		 System.out.println(ex.getMensaje());	 // Encontraremos las excepciones 
	 }
		 catch(InputMismatchException ime) { // Y si no es un numero salta error
				System.out.println("No has introducido un numero");
			}
		
  
	}
	
	
	
	
	
	

}
