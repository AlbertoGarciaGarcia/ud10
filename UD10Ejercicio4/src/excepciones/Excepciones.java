package excepciones;

public class Excepciones extends Exception {
	
	private String mensajeError;
	public Excepciones(String mensajeError) {
		super();
		this.mensajeError = mensajeError;
	}
	
	public String getMensaje() {
		String mensaje = "";
		
		switch(mensajeError) {
		case "Division0": // Haremos una excepcion en caso de intentar dividir entre 0
			mensaje = "No se puede dividir entre 0";
		break;
		case "RaizNegativa": // Y una excepcion en caso de intentar hacer una raiz negativa
			mensaje = "No se puede hacer una ra�z de un n�mero negativo";
		break;
		}
		return mensaje;
	}
	
	

}
