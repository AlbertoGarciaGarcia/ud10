package views;

import java.util.InputMismatchException;
import java.util.Scanner;
import dto.metodosSueltos;
import excepciones.Excepciones;
public class Leer {

	public static void menuCalculadora() throws Excepciones,InputMismatchException  {
		Scanner scanner = new Scanner(System.in);  
		
		System.out.println("Introduce la operacion a realizar : "); // Hacemos un menu
		System.out.println("1. Suma ");
		System.out.println("2. Resta ");
		System.out.println("3. Multiplicacion ");
		System.out.println("4. Potencia ");
		System.out.println("5. Raiz cuadrada ");
		System.out.println("6. Raiz cubica ");
		System.out.println("7. Division ");
		
		int opcion = 0;
		try {
		do {
			System.out.println("Introduce un numero valido");
			
			 opcion = scanner.nextInt(); // Hasta que no se introduzca un valor valido
			 							// Se seguira preguntando
			 							// Todo dentro de un try catch que detecta el error de intrdocir letras
		
		}while(opcion < 0 || opcion > 8);
		
		
		switch (opcion) {
		case 1:
			System.out.println("Introduce el valor a");
			int a = scanner.nextInt();
			System.out.println("Introduce el valor b");
			int b = scanner.nextInt(); // Hacemos la suma
			metodosSueltos.sumaOperacion(a, b);
			break;
		case 2:
			System.out.println("Introduce el valor a");
			 a = scanner.nextInt();
			System.out.println("Introduce el valor b");
			 b = scanner.nextInt(); // La resta
			metodosSueltos.restaOperacion(a, b);
			break;
		case 3:
			System.out.println("Introduce el valor a");
			 a = scanner.nextInt();
				System.out.println("Introduce el valor b");
				 b = scanner.nextInt(); // La multiplicacion
				metodosSueltos.multiplicacionOperacion(a, b);
				break;
		case 4:
			System.out.println("Introduce el valor a");
			 a = scanner.nextInt();
				System.out.println("Introduce el valor b");
				 b = scanner.nextInt(); // La potencia
				metodosSueltos.potenciaOperacion(a, b);
				break;
		case 5:
			System.out.println("Introduce el valor a");
			a = scanner.nextInt();
			 if (a < 0) {
				 throw new excepciones.Excepciones("RaizNegativa");
				 
			 }
			metodosSueltos.raizCuadrada(a); // La raiz teniendo en cuenta la excepcion
				break;
		case 6:
			System.out.println("Introduce el valor a");
			a = scanner.nextInt();
			 if (a < 0) {
				 throw new excepciones.Excepciones("RaizNegativa");
				 
			 }
			metodosSueltos.raizCubica(a); // La raiz teniendo en cuenta la excepcion
			break;
		case 7:
			System.out.println("Introduce el valor a");
			 a = scanner.nextInt();
			 if (a == 0) {
				 throw new excepciones.Excepciones("Division0"); // La division teniendo en cuenta que no se puede dividr entre 0
				 
			 }
				System.out.println("Introduce el valor b");
				 b = scanner.nextInt();
				 if (b == 0) {
					 throw new excepciones.Excepciones("Division0");
					
					 
				 }
				 metodosSueltos.divisionOperacion(a, b);
				 
		}
		}
		 
		 catch(InputMismatchException ime) { // Y si no es un numero salta error
				System.out.println("No has introducido un numero");
			}
		catch(Excepciones ex) {
			 System.out.println(ex.getMensaje());	 
		 }
		
	}
	
	
}
