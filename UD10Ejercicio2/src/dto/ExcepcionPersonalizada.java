package dto;

public class ExcepcionPersonalizada extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String mensajeError;
	public ExcepcionPersonalizada(String mensajeError) {
		super(); // Haremos una funcion para que se llame al mensaje de error dependiendo del pasado 
		this.mensajeError = mensajeError;
	}
	
	public String getMensaje() {
		String mensaje = "";
		
		switch(mensajeError) {
		case "noes6": // En caso de que el mensaje de error sea noes6 se lanza el siguiente mensaje
			mensaje = "El numero no es 6";
		break;
		case "es6": // Lo mismo con el es6
			mensaje = "El numero es 6";
		break;
		}
		return mensaje;
	}

}
