
import dto.ExcepcionPersonalizada;
public class ExcepcionMainApp {

	public static void main(String[] args) {
 
		int numero = 7; // Declaramos el numero
		
		try {
			numero = 5; // Le damos como valor de ejemplo el 5
			if(numero != 6) { // Si el numero no es 6, llamamos al mensaje  de error noes6
				throw new ExcepcionPersonalizada("noes6");
			}
			else if(numero == 6) { // Si es 6, llamamos al mensaje de erorr es6
				throw new ExcepcionPersonalizada("es6");
			}
		} catch (ExcepcionPersonalizada ex) { // Haremos un catch de la excepcion
			System.out.println(ex.getMensaje()); // Y lo impriremos
		}
	

	}

}
