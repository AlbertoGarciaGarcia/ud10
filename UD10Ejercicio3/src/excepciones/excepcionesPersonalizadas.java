package excepciones;

public class excepcionesPersonalizadas extends Exception{

	private static final long serialVersionUID = 1L;
	private String mensajeError;
	public excepcionesPersonalizadas(String mensajeError) { // Crearemos el metodo para que pase como valor el mensaje de error pasado por usuario
		super();
		this.mensajeError = mensajeError;
	}
	
	public String getMensaje() {
		String mensaje = "";
		
		switch(mensajeError) {
		case "numeroPar": // Si es par pasaremos el paraemtro numeroPar
			mensaje = "El numero  es Par";
		break;
		case "numeroImpar": // Si es impar el esIMpar
			mensaje = "El numero es Impar";
		break;
		}
		return mensaje;
	}

}
