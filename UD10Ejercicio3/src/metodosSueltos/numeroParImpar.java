package metodosSueltos;
import excepciones.excepcionesPersonalizadas;;
public class numeroParImpar {
	
	
	public static void parOImpar() {
		
		
		try {
			int numeroRandom = random.generarRandom();	
			System.out.println(numeroRandom);
			if(numeroRandom % 2 == 0) { // Si el residuo es 0 
				throw new excepcionesPersonalizadas("numeroPar"); // Lanzamos la excepcion 0		
		}
		else {
			throw new excepcionesPersonalizadas("numeroImpar"); // Y si no es un impar
		}
			
		}catch(excepcionesPersonalizadas ex) { // Y entonces lanza el mensaje
			System.out.println(ex.getMensaje());
		}
		
		
	}

}
